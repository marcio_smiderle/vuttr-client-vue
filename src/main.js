import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import App from './App.vue';

// Replace "tool" with the name of the resource type
import tool from './store/modules/tool/';
import toolRoutes from './router/tool';

Vue.use(Vuex);
Vue.use(VueRouter);

const store = new Vuex.Store({
  modules: {
    tool
  }
});

const router = new VueRouter({
  mode: 'history',
  routes: [
      ...toolRoutes
  ]
});

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
});
