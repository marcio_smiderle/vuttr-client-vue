export const TOOL_SHOW_RESET = 'TOOL_SHOW_RESET'
export const TOOL_SHOW_SET_ERROR = 'TOOL_SHOW_SET_ERROR'
export const TOOL_SHOW_SET_RETRIEVED = 'TOOL_SHOW_SET_RETRIEVED'
export const TOOL_SHOW_TOGGLE_LOADING = 'TOOL_SHOW_TOGGLE_LOADING'
