import ToolList from '../components/tool/List'
import ToolCreate from '../components/tool/Create'
import ToolUpdate from '../components/tool/Update'
import ToolShow from '../components/tool/Show'

export default [
  { name: 'ToolList', path: '/tools/', component: ToolList },
  { name: 'ToolCreate', path: '/tools/create', component: ToolCreate },
  { name: 'ToolUpdate', path: '/tools/edit/:id', component: ToolUpdate },
  { name: 'ToolShow', path: '/tools/show/:id', component: ToolShow }
]
